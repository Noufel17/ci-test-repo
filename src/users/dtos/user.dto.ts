export class CreateUserDto {
  username: string;
  password: string;
}
export class UpdatedUserDto {
  username: string;
  password: string;
}
export class ProfileDetailsDto {
  firstname: string;
  lastname: string;
  age: number;
  dateOfBirth: string;
}
export class PostDetailsDto {
  title: string;
  description: string;
  releaseDate: string;
}
